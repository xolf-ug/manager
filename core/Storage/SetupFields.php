<?php

$setupFields = [
    'Einstellungen' => [
        [
            'label' => 'Titel der Webseite (aktuell Manager)',
            'name' => 'manager-title',
            'typ' => 'text'
        ]
    ],
    'Unternehmen' => [
        [
            'label' => 'Name',
            'name' => 'company-name',
            'typ' => 'text'
        ],
        [
            'label' => 'Geschäftsführer',
            'name' => 'company-ceo',
            'typ' => 'text'
        ],
        [
            'label' => 'Straße und Hausnummer',
            'name' => 'company-street',
            'typ' => 'text'
        ],
        [
            'label' => 'Postleitzahl',
            'name' => 'company-plz',
            'typ' => 'text'
        ],
        [
            'label' => 'Stadt',
            'name' => 'company-city',
            'typ' => 'text'
        ]
    ],
    'Steuern und Bank' => [
        [
            'label' => 'Handelsregister',
            'name' => 'company-hr',
            'typ' => 'text'
        ],
        [
            'label' => 'Handelsregister Eintrag',
            'name' => 'company-grb',
            'typ' => 'text'
        ],
        [
            'label' => 'Geldinstitut',
            'name' => 'company-bank',
            'typ' => 'text'
        ],
        [
            'label' => 'IBAN',
            'name' => 'company-iban',
            'typ' => 'text'
        ],
        [
            'label' => 'BIC',
            'name' => 'company -bic',
            'typ' => 'text'
        ]
    ],
    'Kontakt' => [
        [
            'label' => 'Telefon',
            'name' => 'company-tel',
            'typ' => 'text'
        ],
        [
            'label' => 'Fax',
            'name' => 'company-fax',
            'typ' => 'text'
        ],
        [
            'label' => 'Website',
            'name' => 'company-web',
            'typ' => 'text'
        ],
        [
            'label' => 'E-Mail',
            'name' => 'company-mail',
            'typ' => 'text'
        ]
    ]
    
        
];