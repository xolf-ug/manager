<?php include 'bootstrap.php' ?>
<?php include 'style/head.php' ?>

<style>
    .wizard {
        margin: 20px auto;
        background: #fff;
    }

    .wizard .nav-tabs {
        position: relative;
        margin: 40px auto;
        margin-bottom: 0;
        border-bottom-color: #e0e0e0;
    }

    .wizard > div.wizard-inner {
        position: relative;
    }

    .connecting-line {
        height: 2px;
        background: #e0e0e0;
        position: absolute;
        width: 80%;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: 50%;
        z-index: 1;
    }

    .wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
        color: #555555;
        cursor: default;
        border: 0;
        border-bottom-color: transparent;
    }

    span.round-tab {
        width: 70px;
        height: 70px;
        line-height: 70px;
        display: inline-block;
        border-radius: 100px;
        background: #fff;
        border: 2px solid #e0e0e0;
        z-index: 2;
        position: absolute;
        left: 0;
        text-align: center;
        font-size: 25px;
    }
    span.round-tab i{
        color:#555555;
    }
    .wizard li.active span.round-tab {
        background: #fff;
        border: 2px solid #5bc0de;

    }
    .wizard li.active span.round-tab i{
        color: #5bc0de;
    }

    span.round-tab:hover {
        color: #333;
        border: 2px solid #333;
    }

    .wizard .nav-tabs > li {
        width: 25%;
    }

    .wizard li:after {
        content: " ";
        position: absolute;
        left: 46%;
        opacity: 0;
        margin: 0 auto;
        bottom: 0px;
        border: 5px solid transparent;
        border-bottom-color: #5bc0de;
        transition: 0.1s ease-in-out;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 46%;
        opacity: 1;
        margin: 0 auto;
        bottom: 0px;
        border: 10px solid transparent;
        border-bottom-color: #5bc0de;
    }

    .wizard .nav-tabs > li a {
        width: 70px;
        height: 70px;
        margin: 20px auto;
        border-radius: 100%;
        padding: 0;
    }

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

    .wizard .tab-pane {
        position: relative;
        padding-top: 50px;
    }

    .wizard h3 {
        margin-top: 0;
    }

    @media( max-width : 585px ) {

        .wizard {
            width: 90%;
            height: auto !important;
        }

        span.round-tab {
            font-size: 16px;
            width: 50px;
            height: 50px;
            line-height: 50px;
        }

        .wizard .nav-tabs > li a {
            width: 50px;
            height: 50px;
            line-height: 50px;
        }

        .wizard li.active:after {
            content: " ";
            position: absolute;
            left: 35%;
        }
    }
</style>
<div class="well">
    <h1>Einrichten</h1>
    <h4>Vielen Dank, dass Sie sich für den Manager von <a href="https://xolf.info">xolf</a> entschieden haben.</h4>
</div>

<div class="row">
    <form method="post">
        <?php $colWidth = 12 / count($setupFields) ?>
        <?php $i = 0 ?>
        <?php foreach ($setupFields as $title => $row) : ?>
            <?php $i++ ?>
            <div class="col-md-<?php echo $colWidth ?> setup-<?php echo $i ?>" style="<?php echo ($i==1?'':'opacity: 0;') ?>; transition: all 0.5s ease">
                <h4><?php echo $title ?></h4>
                <?php foreach ($row as $input) : ?>
                    <div class="form-group">
                        <label for="<?php echo $input['name'] ?>"><?php echo $input['label'] ?>:</label>
                        <input type="<?php echo $input['type'] ?>" class="form-control" name="<?php echo $input['name'] ?>" id="<?php echo $input['name'] ?>">
                    </div>
                <?php endforeach ?>
                <?php if($i == count($setupFields)) : ?>
                    <input type="submit" value="Einrichten" class="btn btn-primary btn-full btn-block">
                <?php endif ?>
            </div>
        <?php endforeach ?>
        <script>var maxI = <?php echo $i ?>;</script>
    </form>
</div>
<p>
    <button class="btn btn-blue" id="back" style="opacity: 0"><i class="fa fa-angle-left"></i> Zurück</button> <button class="btn btn-primary" id="fwd">Weiter <i class="fa fa-angle-right"></i></button>
</p>

<script>
    $(document).ready(function () {
        var i = 1;

        $("#back,#fwd").click(function () {
            $(".setup-" + i).css('opacity','0');
        });

        $("#back").click(function () {
            i--;
            if(i<=1) {
                i = 1;
                $("#back").css('opacity','0');
            }
        });

        $("#fwd").click(function () {
            $("#back").css('opacity','1');
            i++;
            if(i>maxI) {
                i--;
                $("#fwd").css('opacity','0');
            }
        });

        $("#back,#fwd").click(function () {
            $(".setup-" + i).css('opacity','1');
        });
    });

</script>

<?php include 'style/foot.php' ?>
